import sys
import re

from PyQt5.QtCore import QSize, Qt
from PyQt5.QtSql import QSqlDatabase, QSqlRelationalTableModel, QSqlRelation, QSqlRelationalDelegate
from PyQt5.QtWidgets import (
    QApplication,
    QLineEdit,
    QMainWindow,
    QTableView,
    QVBoxLayout,
    QWidget
)

db = QSqlDatabase("QSQLITE")
db.setDatabaseName("chinook.sqlite")
db.open()


class MainWindow(QMainWindow):
	def __init__(self):
		super().__init__()
		
		container = QWidget()
		layout = QVBoxLayout()

		self.search = QLineEdit()
		self.search.textChanged.connect(self.update_filter)
		self.table = QTableView()

		layout.addWidget(self.search)
		layout.addWidget(self.table)
		container.setLayout(layout)

		self.model = QSqlRelationalTableModel(db=db)

		self.table.setModel(self.model)

		self.model.setTable("Track")
		self.model.setRelation(2, QSqlRelation("Album", "AlbumId", "Title"))
		self.model.setRelation(3, QSqlRelation("MediaType", "MediaTypeId", "Name"))
		self.model.setRelation(4, QSqlRelation("Genre", "GenreId", "Name"))
		
		delegate = QSqlRelationalDelegate(self.table)
		self.table.setItemDelegate(delegate)
		
		self.model.select()

		self.setMinimumSize(QSize(1024, 600))
		self.setCentralWidget(container)
	
	def update_filter(self, s):
		s = re.sub("[\W]+", "", s)
		filter_str = 'Name LIKE "%{}%"'.format(s)
		self.model.setFilter(filter_str)


app = QApplication(sys.argv)
window = MainWindow()
window.show()
app.exec_()

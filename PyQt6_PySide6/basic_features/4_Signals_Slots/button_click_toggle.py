import sys

from PySide6.QtWidgets import (
	QApplication,
	QMainWindow,
	QPushButton
)


class MainWindow(QMainWindow):
	
	def __init__(self):
		super().__init__()
		
		button = QPushButton("Press Me!")
		button.setCheckable(True)
		button.clicked.connect(self.the_button_was_clicked)
		button.clicked.connect(self.the_button_was_toggled)
		
		self.setWindowTitle("My App")
		
		self.setCentralWidget(button)
		
	
	def the_button_was_clicked(self):
		print("Clicked!")
	
	def the_button_was_toggled(self, checked):
		print("Checked?", checked)


app = QApplication(sys.argv)

window = MainWindow()
window.show()

app.exec()

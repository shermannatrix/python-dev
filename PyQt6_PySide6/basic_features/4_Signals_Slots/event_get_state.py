import sys

from PySide6.QtWidgets import (
	QApplication,
	QMainWindow,
	QPushButton
)


class MainWindow(QMainWindow):
	def __init__(self):

		self.button_is_checked = False
		self.setWindowTitle("My App")
		
		self.button = QPushButton("Press Me!")
		self.button.setCheckable(True)
		self.button.released.connect(self.the_button_was_released)
		self.button.setChecked(self.button_is_checked)

	def the_button_was_released(self):
		self.button_is_checked = self.button.isChecked()

		print(self.button_is_checked)


app = QApplication(sys.argv)

window = MainWindow()
window.show()

app.exec()
import sys

from PySide6.QtCore import Qt
from PySide6.QtWidgets import (
	QApplication,
	QHBoxLayout,
	QLabel,
	QMainWindow,
	QPushButton,
	QStackedLayout,
	QVBoxLayout,
	QWidget
)

from layout_colorwidget import Color


class MainWindow(QMainWindow):

	def __init__(self):
		super().__init__()

		self.setWindowTitle("My App")

		pageLayout = QVBoxLayout()
		button_layout = QHBoxLayout()
		self.stackedLayout = QStackedLayout()

		pageLayout.addLayout(button_layout)
		pageLayout.addLayout(self.stackedLayout)

		btn = QPushButton("red")
		btn.pressed.connect(self.activate_tab_1)
		button_layout.addWidget(btn)
		self.stackedLayout.addWidget(Color("red"))

		btn = QPushButton("green")
		btn.pressed.connect(self.activate_tab_2)
		button_layout.addWidget(btn)
		self.stackedLayout.addWidget(Color("green"))

		btn = QPushButton("yellow")
		btn.pressed.connect(self.activate_tab_3)
		button_layout.addWidget(btn)
		self.stackedLayout.addWidget(Color("yellow"))

		widget = QWidget()
		widget.setLayout(pageLayout)
		self.setCentralWidget(widget)
	
	def activate_tab_1(self):
		self.stackedLayout.setCurrentIndex(0)
	
	def activate_tab_2(self):
		self.stackedLayout.setCurrentIndex(1)
	
	def activate_tab_3(self):
		self.stackedLayout.setCurrentIndex(2)


app = QApplication(sys.argv)

window = MainWindow()
window.show()

app.exec()

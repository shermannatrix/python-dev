from PySide6.QtWidgets import QApplication, QWidget

# Only needed for access to command line arguments
import sys

app = QApplication(sys.argv)

# Create a QtWidget, which will be our window
window = QWidget()
window.show()

app.exec()

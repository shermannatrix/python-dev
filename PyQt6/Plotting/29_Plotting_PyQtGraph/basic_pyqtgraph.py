import sys

from PyQt6 import QtWidgets, QtGui
import pyqtgraph as pg


class MainWindow(QtWidgets.QMainWindow):
	def __init__(self):
		super().__init__()

		self.graphWidget = pg.PlotWidget()
		self.setCentralWidget(self.graphWidget)

		hour = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
		temperature = [30, 32, 34, 32, 33, 31, 29, 32, 35, 45]

		# self.graphWidget.setBackground('#bbccaa')
		# self.graphWidget.setBackground((100, 50, 255, 25))
		self.graphWidget.setBackground("w")
		pen = pg.mkPen(color=(255, 0, 0))
		self.graphWidget.plot(hour, temperature, pen=pen, symbol='+', symbolSize=20, symbolBrush=('b'))
		self.graphWidget.setLabel('left', "<span style=\"color:red; font-size: 30px\">Temperature (°C)</span>")
		self.graphWidget.setLabel('bottom', "<span style=\"color:red; font-size: 30px\">Hour (H)</span>")
		# Add legend
		self.graphWidget.addLegend()
		# Add grid
		self.graphWidget.showGrid(x=True, y=True)
		# Set Range
		self.graphWidget.setXRange(0, 10, padding=0)
		self.graphWidget.setYRange(20, 55, padding=0)

		pen = pg.mkPen(color=(255, 0, 0))
		self.graphWidget.plot(hour, 
			temperature, 
			name="Sensor 1",
			pen=pen,
			symbol="+",
			symbolSize=30,
			symbolBrush=("b"))

app = QtWidgets.QApplication(sys.argv)
main = MainWindow()
main.show()
app.exec()

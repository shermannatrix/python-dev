import sys
from random import choice, randint

from PyQt6.QtCore import Qt, QPoint
from PyQt6.QtGui import QPainter, QPixmap, QColor, QPen
from PyQt6.QtWidgets import (
	QApplication, QLabel, QMainWindow
)


class MainWindow(QMainWindow):
	def __init__(self):
		super().__init__()

		self.label = QLabel()
		self.canvas = QPixmap(400, 300)
		self.canvas.fill(Qt.GlobalColor.white)
		self.label.setPixmap(self.canvas)
		self.setCentralWidget(self.label)
		self.draw_something()
	
	def draw_something(self):
		painter = QPainter(self.canvas)
		pen = QPen()
		pen.setWidth(3)
		pen.setColor(QColor("#EB5160"))
		painter.setPen(pen)
		painter.drawRect(50, 50, 100, 100)
		painter.drawRect(60, 60, 150, 100)
		painter.drawRect(70, 70, 100, 150)
		painter.drawRect(80, 80, 150, 100)
		painter.drawRect(90, 90, 100, 150)
		painter.end()
		self.label.setPixmap(self.canvas)



app = QApplication(sys.argv)
window = MainWindow()
window.show()
app.exec()

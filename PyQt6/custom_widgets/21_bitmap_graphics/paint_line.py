import sys

from PyQt6.QtCore import Qt
from PyQt6.QtGui import QPainter, QPixmap
from PyQt6.QtWidgets import QApplication, QLabel, QMainWindow


class MainWindow(QMainWindow):
	def __init__(self):
		super().__init__()

		self.label = QLabel()
		self.canvas = QPixmap(400, 300)
		self.canvas.fill(Qt.GlobalColor.white)
		self.label.setPixmap(self.canvas)
		self.setCentralWidget(self.label)
		self.setMouseTracking(True)

		self.last_x, self.last_y = None, None
	
	def mouseMoveEvent(self, e):
		pos = e.position()
		if self.last_x is None:
			self.last_x = pos.x()
			self.last_y = pos.y()
			return
		
		painter = QPainter(self.canvas)
		painter.drawLine(int(self.last_x), int(self.last_y), int(pos.x()), int(pos.y()))
		painter.end()

		self.label.setPixmap(self.canvas)

		# Update the origin for the next time.
		self.last_x = pos.x()
		self.last_y = pos.y()
	
	def mouseReleaseEvent(self, e):
		self.last_x = None
		self.last_y = None



app = QApplication(sys.argv)
window = MainWindow()
window.show()
app.exec()
